import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Video } from './types';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this
      .http
      .get<Video[]>('http://api.angularbootcamp.com/videos')
      .pipe(
        tap(data => console.log('videos before:', data)),
        map(videos => videos.slice(0, 3)),
        tap(data => console.log('videos after:', data))
      )
    ;
  }
}
