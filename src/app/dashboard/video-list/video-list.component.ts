import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Video } from '../../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[];
  @Output() videoSelected = new EventEmitter<Video>();

  selectedVideo: Video;

  constructor() { }

  ngOnInit() {
  }

  selectVideo(video: Video) {
    this.selectedVideo = video;
    this.videoSelected.emit(video);
  }
}
